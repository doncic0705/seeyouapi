package com.seeyou.seeyouapi.repository;

import com.seeyou.seeyouapi.entity.ProductOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductOrderRepository extends JpaRepository<ProductOrder, Long> {
    Page<ProductOrder> findAllByIdGreaterThanEqualOrderByIdDesc (long page, Pageable pageable);
    Page<ProductOrder> findAllByIdGreaterThanEqualAndIsCompleteTrueOrderByCompleteDateOrderDesc (long page, Pageable pageable);
    List<ProductOrder> findAllByOrderYearAndOrderMonth(int year, String month);
}
