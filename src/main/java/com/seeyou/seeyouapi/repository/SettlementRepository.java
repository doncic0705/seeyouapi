package com.seeyou.seeyouapi.repository;

import com.seeyou.seeyouapi.entity.Settlement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SettlementRepository extends JpaRepository<Settlement, Long> {
    Optional<Settlement> findBySettlementYearAndSettlementMonth(int year, String month);
}
