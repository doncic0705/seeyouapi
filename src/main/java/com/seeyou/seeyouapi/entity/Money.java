package com.seeyou.seeyouapi.entity;

import com.seeyou.seeyouapi.enums.MoneyType;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.model.money.MoneyRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Money {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "staffId", nullable = false)
    private Member member;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private MoneyType moneyType;

    @Column(nullable = false)
    private Double beforeMoney;

    // 국민연금
    @Column(nullable = false)
    private Double nationalPension;

    // 건강보험
    @Column(nullable = false)
    private Double healthInsurance;

    // 장기요양보험
    @Column(nullable = false)
    private Double longTermCareInsurance;

    // 고용보험
    @Column(nullable = false)
    private Double employmentInsurance;

    // 산재보험
    @Column(nullable = false)
    private Double industrialAccidentInsurance;

    // 소득세
    @Column(nullable = false)
    private Double incomeTax;

    // 지방 소득세
    @Column(nullable = false)
    private Double localIncomeTax;

    public void putMoney(MoneyRequest moneyRequest) {
        this.moneyType = moneyRequest.getMoneyType();
        this.beforeMoney = moneyRequest.getBeforeMoney();
        this.nationalPension = moneyRequest.getNationalPension();
        this.healthInsurance = moneyRequest.getHealthInsurance();
        this.longTermCareInsurance = moneyRequest.getLongTermCareInsurance();
        this.employmentInsurance = moneyRequest.getEmploymentInsurance();
        this.industrialAccidentInsurance = moneyRequest.getIndustrialAccidentInsurance();
        this.incomeTax = moneyRequest.getIncomeTax();
        this.localIncomeTax = moneyRequest.getLocalIncomeTax();
    }

    private Money(Builder builder) {

        this.member = builder.member;
        this.moneyType = builder.moneyType;
        this.beforeMoney = builder.beforeMoney;
        this.nationalPension = builder.nationalPension;
        this.healthInsurance = builder.healthInsurance;
        this.longTermCareInsurance = builder.longTermCareInsurance;
        this.employmentInsurance = builder.employmentInsurance;
        this.industrialAccidentInsurance = builder.industrialAccidentInsurance;
        this.incomeTax = builder.incomeTax;
        this.localIncomeTax = builder.localIncomeTax;

    }

    public static class Builder implements CommonModelBuilder<Money> {

        private final Member member;
        private final MoneyType moneyType;
        private final Double beforeMoney;
        private final Double nationalPension;
        private final Double healthInsurance;
        private final Double longTermCareInsurance;
        private final Double employmentInsurance;
        private final Double industrialAccidentInsurance;
        private final Double incomeTax;
        private final Double localIncomeTax;

        public Builder(Member member, MoneyRequest request) {

            this.member = member;
            this.moneyType = request.getMoneyType();
            this.beforeMoney = request.getBeforeMoney();
            this.nationalPension = request.getNationalPension();
            this.healthInsurance = request.getHealthInsurance();
            this.longTermCareInsurance = request.getLongTermCareInsurance();
            this.employmentInsurance = request.getEmploymentInsurance();
            this.industrialAccidentInsurance = request.getIndustrialAccidentInsurance();
            this.incomeTax = request.getIncomeTax();
            this.localIncomeTax = request.getLocalIncomeTax();
        }

        @Override
        public Money build() {
            return new Money(this);
        }
    }
}
