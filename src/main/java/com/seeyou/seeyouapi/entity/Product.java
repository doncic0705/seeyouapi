package com.seeyou.seeyouapi.entity;

import com.seeyou.seeyouapi.enums.ProductType;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.model.product.ProductRequest;
import com.seeyou.seeyouapi.model.product.ProductUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private ProductType productType;

    @Column(nullable = false, length = 20)
    private String productName;

    @Column(nullable = false)
    private Double unitPrice;

    @Column(nullable = false)
    private Boolean isEnabled;

    public void putProduct(ProductUpdateRequest productUpdateRequest) {
        this.productType = productUpdateRequest.getProductType();
        this.productName = productUpdateRequest.getProductName();
        this.isEnabled = productUpdateRequest.getIsEnabled();
    }

    private Product(ProductBuilder productBuilder) {
        this.productType = productBuilder.productType;
        this.productName = productBuilder.productName;
        this.unitPrice = productBuilder.unitPrice;
        this.isEnabled = productBuilder.isEnabled;
    }

    public static class ProductBuilder implements CommonModelBuilder<Product> {
        private final ProductType productType;
        private final String productName;
        private final Double unitPrice;
        private final Boolean isEnabled;

        public ProductBuilder(ProductRequest productRequest) {
            this.productType = productRequest.getProductType();
            this.productName = productRequest.getProductName();
            this.unitPrice = productRequest.getUnitPrice();
            this.isEnabled = true;
        }

        @Override
        public Product build() {
            return new Product(this);
            }
        }
    }

