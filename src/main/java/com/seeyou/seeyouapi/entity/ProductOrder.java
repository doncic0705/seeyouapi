package com.seeyou.seeyouapi.entity;

import com.seeyou.seeyouapi.enums.ProductType;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.model.productOrder.ProductOrderRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "productId", nullable = false)
    private Product product;

    @Column(nullable = false)
    private Integer quantity;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private Integer orderYear;

    @Column(nullable = false)
    private String orderMonth;

    @Column(nullable = false)
    private String orderDay;

    @Column(nullable = false)
    private LocalTime timeOrder;

    private LocalDateTime completeDateOrder;

    @Column(nullable = false)
    private Boolean isComplete;

    public void putProductOrder() {
        this.isComplete = true;
        this.completeDateOrder = LocalDateTime.now();
    }

    private ProductOrder(Builder builder) {
        this.product = builder.product;
        this.quantity = builder.quantity;
        this.price = builder.price;
        this.orderYear = builder.orderYear;
        this.orderMonth = builder.orderMonth;
        this.orderDay = builder.orderDay;
        this.timeOrder = builder.timeOrder;
        this.completeDateOrder = builder.completeDateOrder;
        this.isComplete = builder.isComplete;

    }

    public static class Builder implements CommonModelBuilder<ProductOrder> {
        private final Product product;
        private final Integer quantity;
        private final Double price;
        private final Integer orderYear;
        private final String orderMonth;
        private final String orderDay;
        private final LocalTime timeOrder;
        private final LocalDateTime completeDateOrder;
        private final Boolean isComplete;

        public Builder(Product product, ProductOrderRequest request) {
            this.product = product;
            this.quantity = request.getQuantity();
            this.price = product.getUnitPrice() * quantity;
            this.orderYear = LocalDate.now().getYear();
            this.orderMonth = LocalDate.now().getMonthValue() < 10 ? "0" + LocalDate.now().getMonthValue() : String.valueOf(LocalDate.now().getMonthValue());
            this.orderDay = LocalDate.now().getDayOfMonth() < 10 ? "0" + LocalDate.now().getDayOfMonth() : String.valueOf(LocalDate.now().getDayOfMonth());
            this.timeOrder = LocalTime.now();
            this.completeDateOrder = null;
            this.isComplete = false;
        }
        @Override
        public ProductOrder build() {
            return new ProductOrder(this);
        }
    }
}
