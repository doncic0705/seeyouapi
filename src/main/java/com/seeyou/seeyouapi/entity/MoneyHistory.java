package com.seeyou.seeyouapi.entity;

import com.seeyou.seeyouapi.enums.MoneyType;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.model.money.MoneyHistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MoneyHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "staffId", nullable = false)
    private Member member;

    @Column(nullable = false)
    private Integer moneyYear;

    @Column(nullable = false)
    private Integer moneyMonth;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private MoneyType moneyType;

    @Column(nullable = false)
    private Double beforeMoney;

    // 국민연금
    @Column(nullable = false)
    private Double nationalPension;

    // 건강보험
    @Column(nullable = false)
    private Double healthInsurance;

    // 장기요양보험
    @Column(nullable = false)
    private Double longTermCareInsurance;

    // 고용보험
    @Column(nullable = false)
    private Double employmentInsurance;

    // 산재보험
    @Column(nullable = false)
    private Double industrialAccidentInsurance;

    // 소득세
    @Column(nullable = false)
    private Double incomeTax;

    // 지방 소득세
    @Column(nullable = false)
    private Double localIncomeTax;

    // 급여
    @Column(nullable = false)
    private Double money;

    private MoneyHistory(Builder builder) {

        this.member = builder.member;
        this.moneyYear = builder.moneyYear;
        this.moneyMonth = builder.moneyMonth;
        this.moneyType = builder.moneyType;
        this.beforeMoney = builder.beforeMoney;
        this.nationalPension = builder.nationalPension;
        this.healthInsurance = builder.healthInsurance;
        this.longTermCareInsurance = builder.longTermCareInsurance;
        this.employmentInsurance = builder.employmentInsurance;
        this.industrialAccidentInsurance = builder.industrialAccidentInsurance;
        this.incomeTax = builder.incomeTax;
        this.localIncomeTax = builder.localIncomeTax;
        this.money = builder.money;

    }

    public static class Builder implements CommonModelBuilder<MoneyHistory> {

        private final Member member;
        private final Integer moneyYear;
        private final Integer moneyMonth;
        private final MoneyType moneyType;
        private final Double beforeMoney;
        private final Double nationalPension;
        private final Double healthInsurance;
        private final Double longTermCareInsurance;
        private final Double employmentInsurance;
        private final Double industrialAccidentInsurance;
        private final Double incomeTax;
        private final Double localIncomeTax;
        private final Double money;

        public Builder(Member member, Double money, MoneyHistoryRequest request) {
            this.member = member;
            this.moneyYear = request.getMoneyYear();
            this.moneyMonth = request.getMoneyMonth();
            this.moneyType = request.getMoneyType();
            this.beforeMoney = request.getBeforeMoney();
            this.nationalPension = request.getNationalPension();
            this.healthInsurance = request.getHealthInsurance();
            this.longTermCareInsurance = request.getLongTermCareInsurance();
            this.employmentInsurance = request.getEmploymentInsurance();
            this.industrialAccidentInsurance = request.getIndustrialAccidentInsurance();
            this.incomeTax = request.getIncomeTax();
            this.localIncomeTax = request.getLocalIncomeTax();
            this.money = money - (this.nationalPension + this.healthInsurance + this.longTermCareInsurance + this.employmentInsurance + this.industrialAccidentInsurance + this.incomeTax + this.localIncomeTax);
        }

        @Override
        public MoneyHistory build() {
            return new MoneyHistory(this);
        }
    }
}
