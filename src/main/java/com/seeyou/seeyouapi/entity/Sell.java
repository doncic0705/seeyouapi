package com.seeyou.seeyouapi.entity;

import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.lib.CommonFormat;
import com.seeyou.seeyouapi.model.sell.SellRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Sell {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Long billNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sellProductId", nullable = false)
    private SellProduct sellProduct;

    @Column(nullable = false)
    private Integer quantity;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private Integer sellYear;

    @Column(nullable = false)
    private String sellMonth;

    @Column(nullable = false)
    private String sellDay;

    @Column(nullable = false)
    private LocalTime timeSell;

    private LocalDateTime dateRefund;

    @Column(nullable = false)
    private Boolean isComplete;

    public void putIsCompleteUpdate() {
        this.isComplete = false;
        this.dateRefund = LocalDateTime.now();
    }

    private Sell(Builder builder) {
        this.billNumber = builder.billNumber;
        this.sellProduct = builder.sellProduct;
        this.quantity = builder.quantity;
        this.price = builder.price;
        this.sellYear = builder.sellYear;
        this.sellMonth = builder.sellMonth;
        this.sellDay = builder.sellDay;
        this.timeSell = builder.timeSell;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<Sell> {
        private final Long billNumber;
        private final SellProduct sellProduct;
        private final Integer quantity;
        private final Double price;
        private final Integer sellYear;
        private final String sellMonth;
        private final String sellDay;
        private final LocalTime timeSell;
        private final Boolean isComplete;

        public Builder(SellProduct sellProduct, SellRequest request) {
            this.billNumber = request.getBillNumber();
            this.sellProduct = sellProduct;
            this.quantity = request.getQuantity();
            this.price = request.getPrice();
            this.sellYear = LocalDate.now().getYear();
            this.sellMonth = LocalDate.now().getMonthValue() < 10 ? "0" + LocalDate.now().getMonthValue() : String.valueOf(LocalDate.now().getMonthValue());
            this.sellDay = LocalDate.now().getDayOfMonth() < 10 ? "0" + LocalDate.now().getDayOfMonth() : String.valueOf(LocalDate.now().getDayOfMonth());
            this.timeSell = LocalTime.now();
            this.isComplete = true;
        }

        @Override
        public Sell build() {
            return new Sell(this);
        }
    }
}