package com.seeyou.seeyouapi.model.productOrder;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ProductOrderRequest {
    @ApiModelProperty(notes = "수량", required = true)
    @NotNull
    private Integer quantity;
}
