package com.seeyou.seeyouapi.model.productOrder;

import com.seeyou.seeyouapi.entity.ProductOrder;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductOrderCompleteItem {
    private Long productOrderId;
    private String productName;
    private Integer quantity;
    private Double unitPrice;
    private Double price;
    private String dateOrder;
    private LocalDateTime completeDateOrder;
    private Boolean isComplete;

    private ProductOrderCompleteItem(Builder builder) {
        this.productOrderId = builder.productOrderId;
        this.productName = builder.productName;
        this.quantity = builder.quantity;
        this.unitPrice = builder.unitPrice;
        this.price = builder.price;
        this.dateOrder = builder.dateOrder;
        this.completeDateOrder = builder.completeDateOrder;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<ProductOrderCompleteItem> {
        private final Long productOrderId;
        private final String productName;
        private final Integer quantity;
        private final Double unitPrice;
        private final Double price;
        private final String dateOrder;
        private final LocalDateTime completeDateOrder;
        private final Boolean isComplete;

        public Builder(ProductOrder productOrder) {
            this.productOrderId = productOrder.getId();
            this.productName = productOrder.getProduct().getProductName();
            this.quantity = productOrder.getQuantity();
            this.unitPrice = productOrder.getProduct().getUnitPrice();
            this.price = productOrder.getPrice();
            this.dateOrder = productOrder.getOrderYear() + "-" + productOrder.getOrderMonth() + "-" + productOrder.getOrderDay() + " " +  productOrder.getTimeOrder().getHour() + ":" + productOrder.getTimeOrder().getMinute();
            this.completeDateOrder = productOrder.getCompleteDateOrder();
            this.isComplete = productOrder.getIsComplete();
        }

        @Override
        public ProductOrderCompleteItem build() {
            return new ProductOrderCompleteItem(this);
        }
    }
}
