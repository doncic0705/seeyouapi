package com.seeyou.seeyouapi.model.sell;

import com.seeyou.seeyouapi.entity.Sell;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SellItem {
    @ApiModelProperty(notes = "판매 아이디")
    private Long sellId;

    @ApiModelProperty(notes = "상품 이름")
    private String productName;

    @ApiModelProperty(notes = "수량")
    private Integer quantity;

    @ApiModelProperty(notes = "금액")
    private Double price;

    @Column(nullable = false)
    private Integer sellYear;

    @Column(nullable = false)
    private String sellMonth;

    @Column(nullable = false)
    private String sellDay;

    @Column(nullable = false)
    private LocalTime timeSell;

    @ApiModelProperty(notes = "환불 날짜")
    private String dateRefund;

    @ApiModelProperty(notes = "판매 상태")
    private String isComplete;

    private SellItem(Builder builder) {
        this.sellId = builder.sellId;
        this.productName = builder.productName;
        this.quantity = builder.quantity;
        this.price = builder.price;
        this.sellYear = builder.sellYear;
        this.sellMonth = builder.sellMonth;
        this.sellDay = builder.sellDay;
        this.timeSell = builder.timeSell;
        this.dateRefund = builder.dateRefund;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<SellItem> {
        private final Long sellId;
        private final String productName;
        private final Integer quantity;
        private final Double price;
        private final Integer sellYear;
        private final String sellMonth;
        private final String sellDay;
        private final LocalTime timeSell;
        private final String dateRefund;
        private final String isComplete;

        public Builder(Sell sell) {
            this.sellId = sell.getId();
            this.productName = sell.getSellProduct().getProductName();
            this.quantity = sell.getQuantity();
            this.price = sell.getPrice();
            this.sellYear = sell.getSellYear();
            this.sellMonth = sell.getSellMonth();
            this.sellDay = sell.getSellDay();
            this.timeSell = sell.getTimeSell();
            this.dateRefund = sell.getDateRefund() == null ? "-" : sell.getDateRefund().toString();
            this.isComplete = sell.getIsComplete() ? "O" : "X";
        }

        @Override
        public SellItem build() {
            return new SellItem(this);
        }
    }
}
