package com.seeyou.seeyouapi.model.sell;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SellRequest {
    @ApiModelProperty(notes = "영수증 번호")
    @NotNull
    private Long billNumber;

    @ApiModelProperty(notes = "수량")
    @NotNull
    private Integer quantity;

    @ApiModelProperty(notes = "금액")
    @NotNull
    private Double price;
}
