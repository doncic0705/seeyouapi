package com.seeyou.seeyouapi.model.board;

import com.seeyou.seeyouapi.entity.Board;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.lib.CommonFormat;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardResponse {
    private String title;
    private String content;
    private String dateWrite;

    private BoardResponse(Builder builder) {
        this.title = builder.title;
        this.content = builder.content;
        this.dateWrite = builder.dateWrite;
    }

    public static class Builder implements CommonModelBuilder<BoardResponse> {
        private final String title;
        private final String content;
        private final String dateWrite;

        public Builder(Board board) {
            this.title = board.getTitle();
            this.content = board.getContent();
            this.dateWrite = CommonFormat.convertLocalDateTimeToString(board.getDateWrite());
        }

        @Override
        public BoardResponse build() {
            return new BoardResponse(this);
        }
    }
}
