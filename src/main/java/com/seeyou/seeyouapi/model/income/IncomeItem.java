package com.seeyou.seeyouapi.model.income;

import com.seeyou.seeyouapi.entity.Income;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class IncomeItem {
    @ApiModelProperty(notes = "시퀀스", required = true)
    private Long id;

    @ApiModelProperty(notes = "연도", required = true)
    private Integer incomeYear;

    @ApiModelProperty(notes = "월", required = true)
    private String incomeMonth;

    @ApiModelProperty(notes = "지출항목", required = true)
    private String incomeCategory;

    @ApiModelProperty(notes = "금액", required = true)
    private Double price;

    private IncomeItem(Builder builder) {
        this.id = builder.id;
        this.incomeYear = builder.incomeYear;
        this.incomeMonth = builder.incomeMonth;
        this.incomeCategory = builder.incomeCategory;
        this.price = builder.price;
    }

    public static class Builder implements CommonModelBuilder<IncomeItem> {
        private final Long id;
        private final Integer incomeYear;
        private final String incomeMonth;
        private final String incomeCategory;
        private final Double price;

        public Builder(Income income) {
            this.id = income.getId();
            this.incomeYear = income.getIncomeYear();
            this.incomeMonth = income.getIncomeMonth();
            this.incomeCategory = income.getIncomeCategory();
            this.price = income.getPrice();
        }

        @Override
        public IncomeItem build() {
            return new IncomeItem(this);
        }
    }
}
