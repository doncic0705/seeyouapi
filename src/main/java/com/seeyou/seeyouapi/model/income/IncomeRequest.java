package com.seeyou.seeyouapi.model.income;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class IncomeRequest {
    @ApiModelProperty(notes = "연도", required = true)
    private Integer incomeYear;

    @ApiModelProperty(notes = "월", required = true)
    private String incomeMonth;

    @ApiModelProperty(notes = "지출항목", required = true)
    private String incomeCategory;

    @ApiModelProperty(notes = "금액", required = true)
    private Double price;
}
