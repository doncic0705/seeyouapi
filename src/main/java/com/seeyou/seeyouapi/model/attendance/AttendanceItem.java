package com.seeyou.seeyouapi.model.attendance;

import com.seeyou.seeyouapi.entity.Attendance;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class AttendanceItem {
    private Long id;

    private String name;

    private String dateAttendance;

    private LocalTime timeAttendance;

    private String timeLeaveWork;

    private AttendanceItem(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.dateAttendance = builder.dateAttendance;
        this.timeAttendance = builder.timeAttendance;
        this.timeLeaveWork = builder.timeLeaveWork;
    }

    public static class Builder implements CommonModelBuilder<AttendanceItem> {
        private final Long id;
        private final String name;
        private final String dateAttendance;
        private final LocalTime timeAttendance;
        private final String timeLeaveWork;

        public Builder(Attendance attendance) {
            this.id = attendance.getId();
            this.name = attendance.getMember().getStaffName();
            this.dateAttendance = attendance.getAttendanceYear() + "-" + attendance.getAttendanceMonth() + "-" + attendance.getAttendanceDay();
            this.timeAttendance = attendance.getTimeAttendance();
            this.timeLeaveWork = attendance.getTimeLeaveWork() == null ? "-" : attendance.getTimeLeaveWork().toString();
        }


        @Override
        public AttendanceItem build() {
            return new AttendanceItem(this);
        }
    }

}
