package com.seeyou.seeyouapi.model.sellproduct;

import com.seeyou.seeyouapi.entity.SellProduct;
import com.seeyou.seeyouapi.enums.SellProductType;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class SellProductItem {

    private Long id;

    private SellProductType sellProductType;

    private String productName;

    private Double unitPrice;

    private String isEnabled;


    private SellProductItem(Builder builder) {
        this.id = builder.id;
        this.sellProductType = builder.sellProductType;
        this.productName = builder.productName;
        this.unitPrice = builder.unitPrice;
        this.isEnabled = builder.isEnabled;
    }

    public static class Builder implements CommonModelBuilder<SellProductItem> {

        private final Long id;
        private final SellProductType sellProductType;
        private final String productName;
        private final Double unitPrice;
        private final String isEnabled;

        public Builder(SellProduct sellProduct) {
            this.id = sellProduct.getId();
            this.sellProductType = sellProduct.getSellProductType();
            this.productName = sellProduct.getProductName();
            this.unitPrice = sellProduct.getUnitPrice();
            this.isEnabled = sellProduct.getIsEnabled() ? "O" : "X";
        }


        @Override
        public SellProductItem build() {
            return new SellProductItem(this);
        }
    }
}

