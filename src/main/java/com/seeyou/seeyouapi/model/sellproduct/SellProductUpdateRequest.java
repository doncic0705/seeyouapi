package com.seeyou.seeyouapi.model.sellproduct;

import com.seeyou.seeyouapi.enums.SellProductType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SellProductUpdateRequest {
    @ApiModelProperty(notes = "판매 상품 타입")
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private SellProductType sellProductType;

    @NotNull
    @ApiModelProperty(notes = "판매 상품 이름")
    @Length(min = 2, max = 20)
    private String productName;

    @NotNull
    @ApiModelProperty(notes = "단가")
    private Double unitPrice;

    @NotNull
    @ApiModelProperty(notes = "상태")
    private Boolean isEnabled;
}
