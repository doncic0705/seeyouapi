package com.seeyou.seeyouapi.model.product;

import com.seeyou.seeyouapi.enums.ProductType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ProductRequest {
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private ProductType productType;

    @Length(min = 2, max = 20)
    @NotNull
    private String productName;

    @NotNull
    private Double unitPrice;
}
