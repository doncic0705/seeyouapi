package com.seeyou.seeyouapi.model.member;

import com.seeyou.seeyouapi.entity.Member;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.lib.CommonFormat;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberResponse {

    private String staffName;

    private String memberGroup;

    private String contact;

    private String dateCreate;

    private String dateEnd;

    private String isEnabled;

    private MemberResponse(Builder builder) {

        this.staffName = builder.staffName;
        this.memberGroup = builder.memberGroup;
        this.contact = builder.contact;
        this.dateCreate = builder.dateCreate;
        this.dateEnd = builder.dateEnd;
        this.isEnabled = builder.isEnabled;
    }

    public static class Builder implements CommonModelBuilder<MemberResponse> {

        private final String staffName;

        private final String memberGroup;

        private final String contact;

        private final String dateCreate;

        private final String dateEnd;

        private final String isEnabled;


        public Builder(Member member) {
            this.staffName = member.getStaffName();
            this.memberGroup = member.getMemberGroup().getRankName();
            this.contact = member.getContact();
            this.dateCreate = CommonFormat.convertLocalDateToString(member.getDateCreate());
            this.dateEnd = CommonFormat.convertLocalDateToString(member.getDateEnd());
            this.isEnabled = member.getIsEnabled() ? "O" : "X" ; // ? = if , 앞의 값은 true , 뒤에 값은 false
        }
        @Override
        public MemberResponse build() {
            return new MemberResponse(this);
        }
    }
}
