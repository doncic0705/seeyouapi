package com.seeyou.seeyouapi.model.member;

import com.seeyou.seeyouapi.enums.MemberGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberCreateRequest {
    @ApiModelProperty(notes = "직원명")
    @NotNull
    @Length(min = 2, max = 20)
    private String staffName;

    @ApiModelProperty(notes = "직급")
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private MemberGroup memberGroup;

    @ApiModelProperty(notes = "연락처")
    @NotNull
    @Length(min = 13, max = 13)
    private String contact;

    @ApiModelProperty(notes = "아이디")
    @NotNull
    @Length(min = 5, max = 20)
    private String username;

    @ApiModelProperty(notes = "비밀번호")
    @NotNull
    @Length(min = 8, max = 20)
    private String password;

}
