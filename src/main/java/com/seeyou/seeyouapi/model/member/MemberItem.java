package com.seeyou.seeyouapi.model.member;

import com.seeyou.seeyouapi.entity.Member;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {

    private Long id;
    private String staffName;
    private String memberGroup;
    private String contact;

    private MemberItem(Builder builder) {

        this.id = builder.id;
        this.staffName = builder.staffName;
        this.memberGroup = builder.memberGroup;
        this.contact = builder.contact;

    }

    public static class Builder implements CommonModelBuilder<MemberItem> {

        private final Long id;
        private final String staffName;
        private final String memberGroup;
        private final String contact;

        public Builder(Member member) {
            this.id = member.getId();
            this.staffName = member.getStaffName();
            this.memberGroup = member.getMemberGroup().getRankName();
            this.contact = member.getContact();
        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}
