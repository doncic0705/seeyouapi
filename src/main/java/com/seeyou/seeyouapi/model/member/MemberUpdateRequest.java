package com.seeyou.seeyouapi.model.member;

import com.seeyou.seeyouapi.enums.MemberGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MemberUpdateRequest {
    @ApiModelProperty(notes = "직원명")
    @NotNull
    @Length(min = 2, max = 20)
    private String staffName;

    @ApiModelProperty(notes = "직급")
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private MemberGroup memberGroup;

    @ApiModelProperty(notes = "연락처")
    @NotNull
    @Length(min = 13, max = 13)
    private String contact;

    @ApiModelProperty(notes = "퇴사일")
    private LocalDate dateEnd;

    @ApiModelProperty(notes = "활성화 여부")
    private Boolean isEnabled;
}
