package com.seeyou.seeyouapi.model.money;

import com.seeyou.seeyouapi.entity.Money;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MoneyResponse {

    private String member;

    private String moneyType;

    private Double beforeMoney;

    private Double nationalPension;

    private Double healthInsurance;

    private Double longTermCareInsurance;

    private Double employmentInsurance;

    private Double industrialAccidentInsurance;

    private Double incomeTax;

    private Double localIncomeTax;

    private MoneyResponse(Builder builder) {

        this.member = builder.member;
        this.moneyType = builder.moneyType;
        this.beforeMoney = builder.beforeMoney;
        this.nationalPension = builder.nationalPension;
        this.healthInsurance = builder.healthInsurance;
        this.longTermCareInsurance = builder.longTermCareInsurance;
        this.employmentInsurance = builder.employmentInsurance;
        this.industrialAccidentInsurance = builder.industrialAccidentInsurance;
        this.incomeTax = builder.incomeTax;
        this.localIncomeTax = builder.localIncomeTax;
    }

    public static class Builder implements CommonModelBuilder<MoneyResponse> {

        private final String member;
        private final String moneyType;
        private final Double beforeMoney;
        private final Double nationalPension;
        private final Double healthInsurance;
        private final Double longTermCareInsurance;
        private final Double employmentInsurance;
        private final Double industrialAccidentInsurance;
        private final Double incomeTax;
        private final Double localIncomeTax;

        public Builder(Money money) {
            this.member = money.getMember().getStaffName();
            this.moneyType = money.getMoneyType().getName();
            this.beforeMoney = money.getBeforeMoney();
            this.nationalPension = money.getNationalPension();
            this.healthInsurance = money.getHealthInsurance();
            this.longTermCareInsurance = money.getLongTermCareInsurance();
            this.employmentInsurance = money.getEmploymentInsurance();
            this.industrialAccidentInsurance = money.getIndustrialAccidentInsurance();
            this.incomeTax = money.getIncomeTax();
            this.localIncomeTax = money.getLocalIncomeTax();
        }

        @Override
        public MoneyResponse build() {
            return new MoneyResponse(this);
        }
    }
}
