package com.seeyou.seeyouapi.model.money;

import com.seeyou.seeyouapi.entity.Money;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MoneyItem {

    private String member;

    private String moneyType;

    private Double beforeMoney;

    private MoneyItem(Builder builder) {
        this.member = builder.member;
        this.moneyType = builder.moneyType;
        this.beforeMoney = builder.beforeMoney;
    }

    public static class Builder implements CommonModelBuilder<MoneyItem> {

        private final String member;

        private final String moneyType;

        private final Double beforeMoney;

        public Builder(Money money) {
            this.member = money.getMember().getStaffName();
            this.moneyType = money.getMoneyType().getName();
            this.beforeMoney = money.getBeforeMoney();
        }

        @Override
        public MoneyItem build() {
            return new MoneyItem(this);
        }
    }
}
