package com.seeyou.seeyouapi.model.money;

import com.seeyou.seeyouapi.entity.MoneyHistory;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MoneyHistoryItem {
    private String member;

    private Integer moneyYear;

    private Integer moneyMonth;

    private Double money;

    private MoneyHistoryItem(Builder builder) {
        this.member = builder.member;
        this.moneyYear = builder.moneyYear;
        this.moneyMonth = builder.moneyMonth;
        this.money = builder.money;

    }

    public static class Builder implements CommonModelBuilder<MoneyHistoryItem> {

        private final String member;
        private final Integer moneyYear;
        private final Integer moneyMonth;
        private final Double money;

        public Builder(MoneyHistory moneyHistory) {
            this.member = moneyHistory.getMember().getStaffName();
            this.moneyYear = moneyHistory.getMoneyYear();
            this.moneyMonth = moneyHistory.getMoneyMonth();
            this.money = moneyHistory.getMoney();
        }
        @Override
        public MoneyHistoryItem build() {
            return new MoneyHistoryItem(this);
        }
    }
}
