package com.seeyou.seeyouapi.model.money;

import com.seeyou.seeyouapi.enums.MoneyType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MoneyHistoryRequest {

    @ApiModelProperty(notes = "연도")
    @NotNull
    private Integer moneyYear;

    @ApiModelProperty(notes = "월")
    @NotNull
    private Integer moneyMonth;

    @ApiModelProperty(notes = "급여타입")
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private MoneyType moneyType;

    @ApiModelProperty(notes = "세전금액")
    @NotNull
    private Double beforeMoney;

    @ApiModelProperty(notes = "국민연금")
    @NotNull
    private Double nationalPension;

    @ApiModelProperty(notes = "건강보험")
    @NotNull
    private Double healthInsurance;

    @ApiModelProperty(notes = "장기요양보험")
    @NotNull
    private Double longTermCareInsurance;

    @ApiModelProperty(notes = "고용보험")
    @NotNull
    private Double employmentInsurance;

    @ApiModelProperty(notes = "산재보험")
    @NotNull
    private Double industrialAccidentInsurance;

    @ApiModelProperty(notes = "소득세")
    @NotNull
    private Double incomeTax;

    @ApiModelProperty(notes = "지방 소득세")
    @NotNull
    private Double localIncomeTax;
}
