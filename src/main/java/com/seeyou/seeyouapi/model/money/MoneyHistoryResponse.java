package com.seeyou.seeyouapi.model.money;

import com.seeyou.seeyouapi.entity.MoneyHistory;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MoneyHistoryResponse {
    private String member;

    private Integer moneyYear;

    private Integer moneyMonth;

    private Double beforeMoney;

    private Double nationalPension;

    private Double healthInsurance;

    private Double longTermCareInsurance;

    private Double employmentInsurance;

    private Double industrialAccidentInsurance;

    private Double incomeTax;

    private Double localIncomeTax;

    private Double totalDeductionAmount;

    private Double money;

    private MoneyHistoryResponse(Builder builder) {
        this.member = builder.member;
        this.moneyYear = builder.moneyYear;
        this.moneyMonth = builder.moneyMonth;
        this.beforeMoney = builder.beforeMoney;
        this.nationalPension = builder.nationalPension;
        this.healthInsurance = builder.healthInsurance;
        this.longTermCareInsurance = builder.longTermCareInsurance;
        this.employmentInsurance = builder.employmentInsurance;
        this.industrialAccidentInsurance = builder.industrialAccidentInsurance;
        this.incomeTax = builder.incomeTax;
        this.localIncomeTax = builder.localIncomeTax;
        this.totalDeductionAmount = builder.totalDeductionAmount;
        this.money = builder.money;
    }

    public static class Builder implements CommonModelBuilder<MoneyHistoryResponse> {

        private final String member;
        private final Integer moneyYear;
        private final Integer moneyMonth;
        private final Double beforeMoney;
        private final Double nationalPension;
        private final Double healthInsurance;
        private final Double longTermCareInsurance;
        private final Double employmentInsurance;
        private final Double industrialAccidentInsurance;
        private final Double incomeTax;
        private final Double localIncomeTax;
        private final Double totalDeductionAmount;
        private final Double money;

        public Builder(MoneyHistory moneyHistory) {
            this.member = moneyHistory.getMember().getStaffName();
            this.moneyYear = moneyHistory.getMoneyYear();
            this.moneyMonth = moneyHistory.getMoneyMonth();
            this.beforeMoney = moneyHistory.getBeforeMoney();
            this.nationalPension = moneyHistory.getNationalPension();
            this.healthInsurance = moneyHistory.getHealthInsurance();
            this.longTermCareInsurance = moneyHistory.getLongTermCareInsurance();
            this.employmentInsurance = moneyHistory.getEmploymentInsurance();
            this.industrialAccidentInsurance = moneyHistory.getIndustrialAccidentInsurance();
            this.incomeTax = moneyHistory.getIncomeTax();
            this.localIncomeTax = moneyHistory.getLocalIncomeTax();
            this.totalDeductionAmount = (this.nationalPension + this.healthInsurance + this.longTermCareInsurance + this.employmentInsurance + this.industrialAccidentInsurance + this.incomeTax + this.localIncomeTax);
            this.money =  moneyHistory.getMoney() - totalDeductionAmount;
        }
        @Override
        public MoneyHistoryResponse build() {
            return new MoneyHistoryResponse(this);
        }
    }
}