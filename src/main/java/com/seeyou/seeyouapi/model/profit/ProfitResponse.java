package com.seeyou.seeyouapi.model.profit;

import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfitResponse {
    @ApiModelProperty(notes = "순수익", required = true)
    private Double profit;

    private ProfitResponse(Builder builder) {
        this.profit = builder.profit;
    }

    public static class Builder implements CommonModelBuilder<ProfitResponse> {
        private final Double profit;

        public Builder(double profit) {
            this.profit = profit;
        }

        @Override
        public ProfitResponse build() {
            return new ProfitResponse(this);
        }
    }
}
