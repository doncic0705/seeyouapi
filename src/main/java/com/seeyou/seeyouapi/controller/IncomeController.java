package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.entity.Income;
import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.income.IncomeItem;
import com.seeyou.seeyouapi.model.income.IncomeRequest;
import com.seeyou.seeyouapi.service.IncomeService;
import com.seeyou.seeyouapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "지출 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/income")
public class IncomeController {
    private final IncomeService incomeService;

    @ApiOperation(value = "지출 등록")
    @PostMapping("/data")
    public CommonResult setIncome(@RequestBody @Valid IncomeRequest request) {
        incomeService.setIncome(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "지출 리스트")
    @GetMapping("/all")
    public ListResult<IncomeItem> getList(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        return ResponseService.getListResult(incomeService.getList(page), true);
    }
}
