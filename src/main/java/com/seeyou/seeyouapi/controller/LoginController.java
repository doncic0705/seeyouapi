package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.enums.MemberGroup;
import com.seeyou.seeyouapi.model.common.SingleResult;
import com.seeyou.seeyouapi.model.member.LoginRequest;
import com.seeyou.seeyouapi.model.member.LoginResponse;
import com.seeyou.seeyouapi.service.LoginService;
import com.seeyou.seeyouapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "로그인")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member/login")
public class LoginController {
    private final LoginService loginService;

    @ApiOperation(value = "앱 - 점장 로그인")
    @PostMapping("/app/owner")
    public SingleResult<LoginResponse> doLoginOwner(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_OWNER, loginRequest, "APP"));
    }

    @ApiOperation(value = "앱 - 매니저 로그인")
    @PostMapping("/app/manager")
    public SingleResult<LoginResponse> doLoginManager(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_MANAGER, loginRequest, "APP"));
    }

    @ApiOperation(value = "앱 - 사원 로그인")
    @PostMapping("/web/employee")
    public SingleResult<LoginResponse> doLoginEmployee(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_EMPLOYEE, loginRequest, "APP"));
    }

    @ApiOperation(value = "앱 - 알바 로그인")
    @PostMapping("/web/part-time")
    public SingleResult<LoginResponse> doLoginPartTime(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_PART_TIME, loginRequest, "APP"));
    }
}
