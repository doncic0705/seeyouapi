package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.common.SingleResult;
import com.seeyou.seeyouapi.model.settlement.SettlementRequest;
import com.seeyou.seeyouapi.model.settlement.SettlementResponse;
import com.seeyou.seeyouapi.service.SettlementService;
import com.seeyou.seeyouapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "정산 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/calculation")
public class SettlementController {
    private final SettlementService settlementService;

    @ApiOperation(value = "정산 등록")
    @PostMapping("/data")
    public CommonResult setSettlement(@RequestBody @Valid SettlementRequest request) {
        settlementService.setSettlement(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "정산 리스트")
    @GetMapping("/all")
    public SingleResult<SettlementResponse> getSettlement(@RequestParam("year") int year, @RequestParam("month") String month) {
        return ResponseService.getSingleResult(settlementService.getSettlement(year, month));
    }

    @ApiOperation(value = "정산 수정")
    @PutMapping("/{id}")
    public CommonResult putSettlement(@PathVariable long id) {
        settlementService.putSettlement(id);
        return ResponseService.getSuccessResult();
    }
}
