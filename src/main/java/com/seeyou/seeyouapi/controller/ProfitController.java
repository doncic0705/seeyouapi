package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.model.profit.ProfitResponse;
import com.seeyou.seeyouapi.model.common.SingleResult;
import com.seeyou.seeyouapi.service.ProfitService;
import com.seeyou.seeyouapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "순수익 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/profit")
public class ProfitController {
    private final ProfitService profitService;

    @ApiOperation(value = "순수익 리스트")
    @GetMapping("/all")
    public SingleResult<ProfitResponse> getProfit(@RequestParam("year") int year, @RequestParam("month") String month) {
        return ResponseService.getSingleResult(profitService.getProfit(year, month));
    }
}