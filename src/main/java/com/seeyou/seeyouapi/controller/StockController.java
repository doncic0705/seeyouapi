package com.seeyou.seeyouapi.controller;


import com.seeyou.seeyouapi.entity.Product;
import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.stock.StockItem;
import com.seeyou.seeyouapi.model.stock.StockRequest;
import com.seeyou.seeyouapi.service.ProductService;
import com.seeyou.seeyouapi.service.ResponseService;
import com.seeyou.seeyouapi.service.StockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "재고 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/stock")
public class StockController {
    private final StockService stockService;
    private final ProductService productService;

    @ApiOperation(value = "재고 등록")
    @PostMapping("/data")
    public CommonResult setStock(@RequestBody @Valid StockRequest request, long productId) {
        Product product = productService.getProductData(productId);
        stockService.setStock(product, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "재고 리스트")
    @GetMapping("/list")
    public ListResult<StockItem> getStockList(@RequestParam(value = "page", defaultValue = "1") int page) {
        return ResponseService.getListResult(stockService.getStockList(page), true);
    }

    @ApiOperation(value = "재고 수정")
    @PutMapping("/update/{id}")
    public CommonResult putStock(@PathVariable long id, @RequestBody @Valid StockRequest request) {
        stockService.putStock(id, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "재고 부족 리스트")
    @GetMapping("/lack-list")
    public ListResult<StockItem> getStockLackList() {
       return ResponseService.getListResult(stockService.getStockLackList(), true);
    }
}
