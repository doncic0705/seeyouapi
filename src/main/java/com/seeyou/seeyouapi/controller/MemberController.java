package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.entity.Member;
import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.common.SingleResult;
import com.seeyou.seeyouapi.model.member.*;
import com.seeyou.seeyouapi.service.MemberDataService;
import com.seeyou.seeyouapi.service.ProfileService;
import com.seeyou.seeyouapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "씨유 커피 직원 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberDataService memberDataService;
    private final ProfileService profileService;

    @ApiOperation(value = "맴버 등록")
    @PostMapping("/data")
    public CommonResult setMember(@RequestBody @Valid MemberCreateRequest createRequest) {
        memberDataService.setMember(createRequest.getMemberGroup(), createRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "맴버 리스트")
    @GetMapping("/member-list")

    public ListResult<MemberItem> getMemberList() {
        return ResponseService.getListResult(memberDataService.getMemberList(), true);
    }

    @ApiOperation(value = "맴버 리스트 상세")
    @GetMapping("/member-lists")
    public SingleResult<MemberResponse> getMemberLists(@RequestParam(value = "id") long id) {
        return ResponseService.getSingleResult(memberDataService.getMemberLists(id));
    }

    @ApiOperation(value = "맴버 수정")
    @PutMapping("/member-update")
    public CommonResult putMember(@RequestBody @Valid MemberUpdateRequest updateRequest) {
        Member member = profileService.getMemberData(); // 맴버 데이터가 저장 되어있는 프로필서비스를 불러온다
        memberDataService.putMember(member, updateRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "비밀번호 수정")
    @PutMapping("/password")
    public CommonResult putPassword(@RequestBody @Valid PasswordChangeRequest changeRequest) {
        Member member = profileService.getMemberData();
        memberDataService.putPassword(member, changeRequest);
        return ResponseService.getSuccessResult();
    }
}
