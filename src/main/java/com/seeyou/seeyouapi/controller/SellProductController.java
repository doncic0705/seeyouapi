package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.sellproduct.SellProductItem;
import com.seeyou.seeyouapi.model.sellproduct.SellProductRequest;
import com.seeyou.seeyouapi.model.sellproduct.SellProductUpdateRequest;
import com.seeyou.seeyouapi.service.ResponseService;
import com.seeyou.seeyouapi.service.SellProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "판매 상품 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/sellproduct")
public class SellProductController {
    private final SellProductService sellProductService;

    @PostMapping("/new")
    @ApiOperation(value = "판매 상품 등록")
    public CommonResult setSellProduct(@RequestBody @Valid SellProductRequest sellProductRequest) {
        sellProductService.setSellProduct(sellProductRequest);
        return ResponseService.getSuccessResult();
    }
    @PutMapping("/update/{id}")
    @ApiOperation(value = "판매 상품 수정")
    public CommonResult putSellProduct(@PathVariable long id, @RequestBody @Valid SellProductUpdateRequest request) {
        sellProductService.putSellProduct(id, request);
        return ResponseService.getSuccessResult();
    }
    @GetMapping("/list")
    @ApiOperation(value = "리스트 불러오기")
    public ListResult<SellProductItem> getSellProduct(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        return ResponseService.getListResult(sellProductService.getSellProductList(page), true);
    }
}
