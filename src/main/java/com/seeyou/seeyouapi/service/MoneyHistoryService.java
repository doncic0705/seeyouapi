package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Attendance;
import com.seeyou.seeyouapi.entity.Member;
import com.seeyou.seeyouapi.entity.MoneyHistory;
import com.seeyou.seeyouapi.enums.MoneyType;
import com.seeyou.seeyouapi.exception.CMissingDataException;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.money.MoneyHistoryItem;
import com.seeyou.seeyouapi.model.money.MoneyHistoryRequest;
import com.seeyou.seeyouapi.model.money.MoneyHistoryResponse;
import com.seeyou.seeyouapi.repository.AttendanceRepository;
import com.seeyou.seeyouapi.repository.MoneyHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MoneyHistoryService {
    private final MoneyHistoryRepository moneyHistoryRepository;
    private final AttendanceRepository attendanceRepository;

    public void setMoneyDetails(Member member, MoneyHistoryRequest moneyHistoryRequest) {
        List<Attendance> originList = attendanceRepository.findAllByAttendanceYearAndAttendanceMonthAndMember(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), member);
        double totalMin = 0;
        for (Attendance attendance : originList) {
            totalMin += Math.ceil(ChronoUnit.SECONDS.between(attendance.getTimeAttendance(), attendance.getTimeLeaveWork()) / 60.0);
        }

        // money = detailsRequest의 급여타입이 샐러리 일 때 내림으로 detailsRequest안에 있는 세전금액을 12로 나눔 :
        double money = moneyHistoryRequest.getMoneyType().equals(MoneyType.SALARY) ? Math.floor(moneyHistoryRequest.getBeforeMoney() / 12) : (moneyHistoryRequest.getBeforeMoney() / 60) * totalMin;// 근무시간
        MoneyHistory moneyHistory = new MoneyHistory.Builder(member, money, moneyHistoryRequest).build();
        moneyHistoryRepository.save(moneyHistory);
    }

    public ListResult<MoneyHistoryItem> getMoneyHistories() {
        List<MoneyHistory> originList = moneyHistoryRepository.findAll();

        List<MoneyHistoryItem> result = new LinkedList<>();

        originList.forEach(e -> result.add(new MoneyHistoryItem.Builder(e).build()));

        return ListConvertService.settingResult(result);
    }
    public MoneyHistoryResponse getMoneyHistory(long id) {
        MoneyHistory moneyHistory = moneyHistoryRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new MoneyHistoryResponse.Builder(moneyHistory).build();
    }

    public ListResult<MoneyHistoryItem> getMyMoneyHistories(Member member) {
        List<MoneyHistory> originList = moneyHistoryRepository.findAll();
        List<MoneyHistoryItem> result = new LinkedList<>();
        for (MoneyHistory moneyHistory : originList) {
            result.add(new MoneyHistoryItem.Builder(moneyHistory).build());
        }
        return ListConvertService.settingResult(result);
    }

    public MoneyHistoryResponse getMyMoneyHistory(long id, Member member) {
        MoneyHistory moneyHistory = moneyHistoryRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new MoneyHistoryResponse.Builder(moneyHistory).build();
    }
}
