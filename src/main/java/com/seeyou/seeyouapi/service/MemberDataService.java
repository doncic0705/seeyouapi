package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Member;
import com.seeyou.seeyouapi.enums.MemberGroup;
import com.seeyou.seeyouapi.exception.CMissingDataException;
import com.seeyou.seeyouapi.exception.CNotMatchPasswordException;
import com.seeyou.seeyouapi.exception.CWrongPhoneNumberException;
import com.seeyou.seeyouapi.lib.CommonCheck;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.member.*;
import com.seeyou.seeyouapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;

    public void setFirstMember() {
        String username = "superyerin";
        String password = "12345678";
        String contact = "010-0000-0000";
        boolean isSuperYerin = isNewUsername(username);

        if (isSuperYerin) {
            MemberCreateRequest createRequest = new MemberCreateRequest();
            createRequest.setUsername(username);
            createRequest.setPassword(password);
            createRequest.setStaffName("점장");
            createRequest.setContact(contact);
            setMember(MemberGroup.ROLE_OWNER, createRequest);
        }
    }

    public void setMember(MemberGroup memberGroup, MemberCreateRequest createRequest) {
        if (!CommonCheck.checkUsername(createRequest.getUsername())) throw new CWrongPhoneNumberException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!isNewUsername(createRequest.getUsername())) throw new CWrongPhoneNumberException(); // 중복된 아이디가 존재합니다 던지기

        String password = "seeyoucoffee";
        createRequest.setPassword(passwordEncoder.encode(password));

        Member member = new Member.Builder(memberGroup, createRequest).build();
        memberRepository.save(member);
    }

    private boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);
        return dupCount <= 0;
    }

    public ListResult<MemberItem> getMemberList() {
        List<Member> originList = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        originList.forEach(e -> result.add(new MemberItem.Builder(e).build()));

        return ListConvertService.settingResult(result);
    }

    public MemberResponse getMemberLists(long id) {
        Member member = memberRepository.findById(id).orElseThrow();
        return new MemberResponse.Builder(member).build();
    }

    public void putMember(Member member, MemberUpdateRequest updateRequest) {
        member.putMember(updateRequest);
        memberRepository.save(member);
    }

    public void putPassword(Member member, PasswordChangeRequest changeRequest) {

        if(!passwordEncoder.matches(changeRequest.getCurrentPassword(), member.getPassword())) throw new CNotMatchPasswordException();
        // 현재 비밀번호가 맞지 않으면 던져야 함.

        if (!changeRequest.getChangePassword().equals(changeRequest.getChangePasswordRe())) throw new CNotMatchPasswordException();
        // 변경 할 비밀번호와 변경 비밀번호 확인 값이 일치하지 않으면 던져야함.

        member.putPassword(passwordEncoder.encode(changeRequest.getChangePassword()));
        memberRepository.save(member);

    }

    public Member getMemberData(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new); // member를 id로 엿 바꿔주는 애
    }
}
