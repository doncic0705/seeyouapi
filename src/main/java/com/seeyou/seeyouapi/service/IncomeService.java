package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Income;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.income.IncomeItem;
import com.seeyou.seeyouapi.model.income.IncomeRequest;
import com.seeyou.seeyouapi.repository.IncomeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class IncomeService {
    private final IncomeRepository incomeRepository;

    public void setIncome(IncomeRequest request) {
        Income income = new Income.Builder(request).build();
        incomeRepository.save(income);
    }

    public ListResult<IncomeItem> getList(int page) {
        Page<Income> originList = incomeRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(page));
        List<IncomeItem> result = new LinkedList<>();
        for (Income income : originList.getContent()) {
            result.add(new IncomeItem.Builder(income).build());
        }

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }
}
