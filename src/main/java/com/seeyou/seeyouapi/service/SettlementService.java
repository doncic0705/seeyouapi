package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.ProductOrder;
import com.seeyou.seeyouapi.entity.Settlement;
import com.seeyou.seeyouapi.exception.CMissingDataException;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.settlement.SettlementResponse;
import com.seeyou.seeyouapi.model.settlement.SettlementRequest;
import com.seeyou.seeyouapi.repository.ProductOrderRepository;
import com.seeyou.seeyouapi.repository.SettlementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SettlementService {
    private final SettlementRepository settlementRepository;
    private final ProductOrderRepository productOrderRepository;

    public void setSettlement(SettlementRequest request) {
        List<ProductOrder> originList = productOrderRepository.findAllByOrderYearAndOrderMonth(request.getSettlementYear(), request.getSettlementMonth().toString());
        double price = 0D;
        for (ProductOrder productOrder : originList) {
            price += productOrder.getPrice();
        }
        Settlement settlement = new Settlement.Builder(price, request).build();
        settlementRepository.save(settlement);
    }

    public SettlementResponse getSettlement(int year, String month) {
        Settlement settlement = settlementRepository.findBySettlementYearAndSettlementMonth(year, month).orElseThrow(CMissingDataException::new);
        return new SettlementResponse.Builder(settlement).build();
    }

    public void putSettlement(long id) {
        Settlement settlement = settlementRepository.findById(id).orElseThrow(CMissingDataException::new);
        settlement.putIsComplete();
        settlementRepository.save(settlement);
    }
}