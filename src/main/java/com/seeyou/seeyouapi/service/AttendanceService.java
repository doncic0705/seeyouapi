package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Attendance;
import com.seeyou.seeyouapi.entity.Member;
import com.seeyou.seeyouapi.exception.CMissingDataException;
import com.seeyou.seeyouapi.model.attendance.AttendanceItem;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.repository.AttendanceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AttendanceService {
    private final AttendanceRepository attendanceRepository;

    public void setAttendance(Member member) {
        Optional<Attendance> attendance = attendanceRepository.findByAttendanceYearAndAttendanceMonthAndAttendanceDayAndMember(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), LocalDate.now().getDayOfMonth(), member);
        if (attendance.isEmpty()) {
            Attendance addData = new Attendance.Builder(member).build();
            attendanceRepository.save(addData);
        }
    }

    // 페이징(페이지 넘버를 요구받음)
    public ListResult<AttendanceItem> getAttendanceResult(int pageNum) {
        // attendances는 Page<Attendance> 타입을 가졌다. 
        Page<Attendance> attendances = attendanceRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(pageNum, 20));
        List<AttendanceItem> result = new LinkedList<>();

        for (Attendance attendance : attendances.getContent()) {
            result.add(new AttendanceItem.Builder(attendance).build());

        }
        return ListConvertService.settingResult(result, attendances.getTotalElements(), attendances.getTotalPages(), attendances.getPageable().getPageNumber());
    }
    public void putLeaveWork(Member member) {
        Optional<Attendance> attendance = attendanceRepository.findByAttendanceYearAndAttendanceMonthAndAttendanceDayAndMember(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), LocalDate.now().getDayOfMonth(), member);
        if (attendance.isEmpty()) throw new CMissingDataException();
        Attendance originData = attendance.get();
        originData.leaveWork();
        attendanceRepository.save(originData);
    }
}
