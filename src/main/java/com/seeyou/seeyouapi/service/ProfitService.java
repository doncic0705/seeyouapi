package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Income;
import com.seeyou.seeyouapi.entity.Sell;
import com.seeyou.seeyouapi.model.profit.ProfitResponse;
import com.seeyou.seeyouapi.repository.IncomeRepository;
import com.seeyou.seeyouapi.repository.SellRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProfitService {
    private final SellRepository sellRepository;
    private final IncomeRepository incomeRepository;

    public ProfitResponse getProfit(int year, String month) {
        List<Sell> sellList = sellRepository.findAllBySellYearAndSellMonth(year, month);
        List<Income> incomeList = incomeRepository.findAllByIncomeYearAndIncomeMonth(year, month);
        double sells = 0D;
        double incomes = 0D;
        for (Sell sell : sellList) {
            sells += sell.getPrice();
        }
        for (Income income : incomeList) {
            incomes += income.getPrice();
        }
        return new ProfitResponse.Builder(sells - incomes).build();
    }
}
